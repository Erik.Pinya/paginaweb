function RelojDigital() {

    var fecha = new Date();
    var horas = fecha.getHours();
    var minutos = fecha.getMinutes();
    var T_segundos = fecha.getSeconds();
  
    getDecimal(horas, minutos, T_segundos);
  
    setTimeout(function () {
      RelojDigital();
    }, 1000);
  }
  
  function getDecimal(hrs, num, min) { 
    var hora = Math.floor(hrs / 10);
    var hora2 = hrs % 10;
    var minuto = Math.floor(min / 10);
    var minuto2 = min % 10;
    var segundo = Math.floor(num / 10);
    var segundo2 = num % 10;
  
    Reloj(hora, 'canva1');
    Reloj(hora2, 'canva2');
    Reloj(minuto, 'canva5');
    Reloj(minuto2, 'canva6');
    Reloj(segundo, 'canva3');
    Reloj(segundo2, 'canva4');

  }
  
  function Reloj(segundos, stringReloj) {
    var canvas = document.getElementById(stringReloj);
    if (canvas.getContext) {
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, 500, 500);
      let T_segundos = parseInt(segundos);
      switch (T_segundos) {
        case 1:
          num1();
          break;
        case 2:
          num2();
          break;
        case 3:
          num3();
          break;
        case 4:
          num4();
          break;
        case 5:
          num5();
          break;
        case 6:
          num6();
          break;
        case 7:
          num7();
          break;
        case 8:
          num8();
          break;
        case 9:
          num9();
          break;
        default:
          num0();
          break;
      }
    }
    function A() {
      ctx.fillStyle = 'black';
      ctx.fillRect(40, 10, 100, 20);
    }
    function B() {
      ctx.fillStyle = 'black';
      ctx.fillRect(140, 30, 20, 100);
    }
    function C() {
      ctx.fillStyle = 'black';
      ctx.fillRect(140, 150, 20, 100);
    }
    function D() {
      ctx.fillStyle = 'black';
      ctx.fillRect(40, 250, 100, 20);
    }
    function E() {
      ctx.fillStyle = 'black';
      ctx.fillRect(20, 150, 20, 100);
    }
    function F() {
      ctx.fillStyle = 'black';
      ctx.fillRect(20, 30, 20, 100);
    }
    function G() {
      ctx.fillStyle = 'black';
      ctx.fillRect(40, 130, 100, 20);
    }

    function num0() {
      B();
      C();
      A();
      F();
      E();
      D();
    }
    function num1() {
      B();
      C();
    }
    function num2() {
      A();
      B();
      G();
      E();
      D();
    }
    function num3() {
      A();
      B();
      C();
      G();
      D();
    }
    function num4() {
      F();
      G();
      B();
      C();
    }
    function num5() {
      A();
      F();
      G();
      C();
      D();
    }
    function num6() {
      A();
      F();
      G();
      C();
      D();
      E();
    }
    function num7() {
      B();
      C();
      A();
    }
    function num8() {
      A();
      F();
      G();
      C();
      D();
      E();
      B();
    }
    function num9() {
      F();
      G();
      B();
      C();
      A();
    }
  }

 
